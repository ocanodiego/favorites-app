<?php

namespace App;

use App\User;
use App\Photo;
use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    public function photo(){
    	return $this->belongsTo(Photo::class);
    }

    public function user(){
    	return $this->belongsTo(User::class);
    }
}
