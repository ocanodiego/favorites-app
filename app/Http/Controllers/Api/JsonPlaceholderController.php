<?php

namespace App\Http\Controllers\Api;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

/**
* This Controller will help us against CORS requests
*/

class JsonPlaceholderController extends Controller
{

	function __construct()
	{
		$this->http = new \GuzzleHttp\Client;
	}

    public function photos(Request $request){

    	$params = $request->except('csrf_token');

		$response = $this->http->request('GET', 'https://jsonplaceholder.typicode.com/photos?'.http_build_query($params));

		return $response;

    }
}
