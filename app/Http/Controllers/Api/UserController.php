<?php

namespace App\Http\Controllers\Api;

use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function mostActive(){

    	$today = Carbon::today();

		return User::whereHas('favorites', function($query) use ($today){

			$query->where('favorites.created_at', '>=', $today->subWeek());
			
		})->withCount('favorites')->limit(User::DEFAULT_QUERY_LIMIT)->get();

    }
}
