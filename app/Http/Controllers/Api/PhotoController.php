<?php

namespace App\Http\Controllers\Api;

use App\Photo;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ToggleFavoriteRequest;

class PhotoController extends Controller
{
    public function toggleFavorite(ToggleFavoriteRequest $request){

    	$photo = Photo::where('jsonplaceholder_photo_id', $request->input('photo_id'))->first();

    	if($photo){

    		Auth::user()->favorites()->toggle($photo->id);

    	}else{

    		$photo = Photo::create(array_merge($request->all(), [
    			'jsonplaceholder_photo_id' => $request->input('photo_id')
    		]));

    		Auth::user()->favorites()->toggle($photo->id);

    	}

    	return [
            'status' => 'success',
            'favorited' => Auth::user()->favorites()->where('id', $photo->id)->exists()
        ];
    	
    }

    public function latestFavorites(){

        $today = Carbon::today();

        return Photo::whereHas('favorites', function($query) use ($today){

            $query->where('favorites.created_at', '>=', $today->subWeek());
            
        })->withCount('favorites')->limit(Photo::DEFAULT_QUERY_LIMIT)->get();
        
    }

    public function getAuthUserFavorites(){

    	return Auth::user()->favorites;
    	
    }
}
