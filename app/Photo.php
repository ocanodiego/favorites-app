<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Photo extends Model
{

	const DEFAULT_QUERY_LIMIT = 50;

	protected $fillable = [
		'jsonplaceholder_photo_id',
		'title',
		'url',
		'thumbnail_url'
	];

    public function favorites(){

    	return $this->belongsToMany(User::class, 'favorites')->withTimestamps();;
    
    }
}
