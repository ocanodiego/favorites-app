@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div id="app" class="card">
                <div class="card-header">Feed</div>
                <div class="card-body">
                    <feed></feed>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
