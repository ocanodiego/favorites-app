<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function(){

	Route::get('jsonplaceholder/photos', 'Api\JsonPlaceholderController@photos')->name('jsonplaceholder.photos');

	Route::post('photos/toggle-favorite', 'Api\PhotoController@toggleFavorite')->name('photos.toggle-favorite');

	Route::get('photos/auth-user-favorites', 'Api\PhotoController@getAuthUserFavorites')->name('photos.auth-user-favorites');


});

Route::get('photos/favorites/latest', 'Api\PhotoController@latestFavorites');

Route::get('users/most-active', 'Api\UserController@mostActive');
